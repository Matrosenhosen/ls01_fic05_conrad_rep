﻿import java.util.Scanner;

class Fahrkartenautomat {

	private static Scanner tastatur = new Scanner(System.in);
	private static String ausgabe = "BRUUUUUHHHHHH...";

	public static void main(String[] args) {

		int zuZahlenderBetrag = 0; // Veränderung von double zu int im gesamten Programm, um Rechenfehler zu
									// umgehen
		int eingezahlterGesamtbetrag = 0;
		char auswahl = '1'; // char für menüführung
		// Entfernung der eingeworfeneMünze und rückgabebetrag Variablen Aufgrund von
		// Speicheroptimierung
		System.out.println("Fahrkartenbestellvorgang: \n=============");
		while (auswahl != '9') {
			System.out.println("\nWählen Sie:");
			System.out.println("Einzelfahrschein (1)\nTageskarte (2)\nGruppenkarte (3)\nBezahlen(9)");
			System.out.print("\nIhre Wahl:");
			if ((auswahl = tastatur.next().charAt(0)) == '1' || auswahl == '2' || auswahl == '3')
				zuZahlenderBetrag += fahrkartenbestellungErfassen(auswahl);
			else if(auswahl != '9'){
				System.out.println("Ungültige Eingabe");
			}
		}
		eingezahlterGesamtbetrag = fahrkarteBezahlen(zuZahlenderBetrag);
		// Fahrscheinausgabe
		// -----------------
		fahrkarteAusgeben();

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		geldRueckgabe(eingezahlterGesamtbetrag - zuZahlenderBetrag);
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Thank you for traveling with Deutsche Bahn.");

	}

	private static void fahrkarteAusgeben() { // Ausgabe des Fahrscheins
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < ausgabe.length(); i++) {
			System.out.print(ausgabe.charAt(i));
			warten(250);
		}
		System.out.println("\n\n");
	}

	private static void warten(int millisekunden) { // das Programm wartet n millisekunde
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void geldRueckgabe(int zahl) { // ausgabe des Rückgeldes
		int[] muenzen = { 200, 100, 50, 20, 10, 5, 2, 1 }; // Die verschiedenen Münzarten, die der Automat ausgeben kann
		if (zahl > 0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + euroAusgabe(zahl) + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");
			for (int i = 0; i < muenzen.length; i++) {
				while (zahl >= muenzen[i]) {
					muenzeAusgeben(muenzen[i]);
					zahl -= muenzen[i];
				}
			}
		}

	}

	private static void muenzeAusgeben(int betrag) {// Konsolenausgabe der Münzen mit passender währung
		if (betrag >= 100)
			System.out.println((betrag / 100) + "€");
		else
			System.out.println(betrag + "ct");
	}

	// Geldeinwurf
	private static int fahrkarteBezahlen(int zuZahlen) {
		int gezahlt = 0; // der Betrag, der eingezahlt wurde
		while (gezahlt < zuZahlen) {
			System.out.println("\nNoch zu zahlen: " + euroAusgabe(zuZahlen - gezahlt));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			gezahlt += doubleAlsInt(tastatur.nextDouble());
		}
		return gezahlt;
	}

	private static int fahrkartenbestellungErfassen(char auswahl) { // Betragseingabe
		int betrag = 0; // der zu zahlende Betrag in cent
		final int EINZEL = 290;
		final int TAGES = 1000;
		final int GRUPPE = 1500;
//		System.out.print("Zu zahlender Betrag (EURO): ");
//		if ((betrag = doubleAlsInt(tastatur.nextDouble())) < 0) {
//			System.out.println("Preis kann nicht negativ sein!");
//			betrag *= -1;
//		}

		switch (auswahl) {
		case '1':
			betrag = EINZEL;
			break;
		case '2':
			betrag = TAGES;
			break;
		case '3':
			betrag = GRUPPE;
			break;
		}
		int anz;
		do {
			System.out.println("\nAnzahl der Tickets: ");
			if ((anz = tastatur.nextInt()) <= 10 && anz > 0) {
				betrag *= anz; // Multiplizieren des betrages mit der Anzahl der Tickets
				return betrag;
			} else
				System.out.println("Es können nur 1 bis 10 Tickets ausgegeben werden!");
		} while (true);
	}

	private static String euroAusgabe(int zahl) { // Ausgabe mit zwei Nachkommastellen
		String euro = Double.toString(((double) zahl) / 100); // Der Wert als String um die Zahl als Zeichenkette zu
																// benutzen
		StringBuilder sb = new StringBuilder(euro); // StringBulder um effizienter mit Strings zu arbeiten, damit die
													// Laufzeit kurz bleibt
		if (euro.charAt(euro.length() - 2) != '0' && euro.charAt(euro.length() - 3) != '.') {
			sb.append('0');
		}
		sb.append('€');
		euro = sb.toString();
		return euro;
	}

	private static int doubleAlsInt(double zahl) { // umwandlung eines double geldwertes in int
		int r = (int) (Math.round(zahl * 100)); // int zum speichern der Ganzzahl, die berechnet wird
		return r;
	}
}