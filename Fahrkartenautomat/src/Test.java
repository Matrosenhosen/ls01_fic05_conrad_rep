
public class Test {

	public static void main(String[] args) {
		double d = 2;
		System.out.println(d);
		
		String euro = Double.toString(d);
    	if(euro.charAt(euro.length()-2) != '0'&& euro.charAt(euro.length()-3) != '.') {
    		StringBuilder sb = new StringBuilder();
    		sb.append(euro);
    		sb.append('0');
    		euro = sb.toString();
    	}
    	System.out.println(euro);
	}

}
