
public class Formatierung {
	public static void main(String args[]) {
		// aufgabe 1

		System.out.println("Aufgabe 1:\n");
		System.out.println("   **   ");
		System.out.println("*      *");
		System.out.println("*      *");
		System.out.println("   **   ");
		// aufgabe 2

		System.out.println("\nAufgabe 2:\n");
		String f = "%4s%n";
		String n = "%-5s= ";
		String z = "%-19s=";
		System.out.printf(n, "0!");
		System.out.printf(z, "");
		System.out.printf(f, "1");

		System.out.printf(n, "1!");
		System.out.printf(z, "1");
		System.out.printf(f, "1");

		System.out.printf(n, "2!");
		System.out.printf(z, "1 * 2");
		System.out.printf(f, "2");

		System.out.printf(n, "3!");
		System.out.printf(z, "1 * 2 * 3");
		System.out.printf(f, "6");

		System.out.printf(n, "4!");
		System.out.printf(z, "1 * 2 * 3 * 4");
		System.out.printf(f, "24");

		System.out.printf(n, "5!");
		System.out.printf(z, "1 * 2 * 3 * 4 * 5");
		System.out.printf(f, "120");

		// aufgabe 3

		System.out.println("\nAufgabe 3:\n");

		String l = "%-12d|";
		String r = "%10.2f%n";
		System.out.printf("%-12s|", "Fahrenheit");
		System.out.printf("%10s%n", "Celsius");
		for (int i = 0; i < 23; i++) {
			System.out.print("-");
		}
		System.out.println("");
		System.out.printf(l, -20);
		System.out.printf(r, -28.8889);

		System.out.printf(l, -10);
		System.out.printf(r, -23.3333);

		System.out.printf(l, 0);
		System.out.printf(r, -17.7778);

		System.out.printf(l, 20);
		System.out.printf(r, -6.6667);

		System.out.printf(l, 30);
		System.out.printf(r, -1.1111);
	}
}
